import { API_KEY } from '@env'

const Config = {
  API_URL: 'https://api.mixcloud.com',
}

export default Config
