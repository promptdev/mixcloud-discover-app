import React, {
  useRef,
} from 'react'
import {
  Text,
  TextInput,
  TouchableOpacity,
  View
} from 'react-native'

import Location from 'src/Assets/Svg/location.svg'

import Styles from './styles'

const Nav = ( {
  city,
  cityHandler,
  mode,
  modeHandler,
  tags,
  tagsHandler
} ) => {

  // button styles
  const buttonStyles = Styles.navButton
  const buttonActiveStyles = {
    ...Styles.navButton,
    ...Styles.navButtonActive,
  }

  // button text styles
  const buttonTextStyles = Styles.navUiText
  const buttonActiveTextStyles = {
    ...Styles.navUiText,
    ...Styles.navUiTextActive,
  }

  // text input styles
  const textInputStyles = {
    ...Styles.navUiText,
    ...Styles.navUiTextInput,
  }

  // render text input helper
  const renderTextInput = ( { placeholder, handler } ) => {
    return(
      <TextInput
        autoCapitalize='none'
        autoCompleteType='off'
        autoCorrect={ false }
        clearButtonMode='while-editing'
        defaultValue={ placeholder }
        onChangeText={ handler }
        onFocus={ inputEvent => inputEvent.target.clear() }
        style={ textInputStyles } />
    )
  }

  // render button helper
  const renderButton = ( { modeLabel } ) => {
    return(
      <TouchableOpacity
        disabled={ mode === modeLabel ? true : false }
        style={ mode === modeLabel ? buttonActiveStyles : buttonStyles }
        onPress={ modeHandler }>
          <Text style={ mode === modeLabel ? buttonActiveTextStyles : buttonTextStyles }>
            { modeLabel }
          </Text>
      </TouchableOpacity>
    )
  }

  return (
    <View style={ Styles.navView }>
      <View style={ Styles.navUiView }>
        <View style={ Styles.navUiInputIcon }>
          <Location width={ 13 } height={ 16 } />
        </View>
        { renderTextInput( {
            placeholder: `${ city }`,
            handler: cityHandler,
        } ) }
      </View>
      <View style={ Styles.navUiView }>
        <View style={ Styles.navUiInputIcon }>
          <Text style={ Styles.navUiIconText }>
            #
          </Text>
        </View>
        { renderTextInput( {
            placeholder: 'hashtags',
            handler: tagsHandler
        } ) }
      </View>
      <View style={ { ...Styles.navUiView, ...Styles.navUiViewEnd } }>
        <View style={ Styles.navUiModeView }>
          { renderButton( { modeLabel: 'latest' } ) }
          { renderButton( { modeLabel: 'popular' } ) }
        </View>
      </View>
    </View>
  )
}

export default Nav
