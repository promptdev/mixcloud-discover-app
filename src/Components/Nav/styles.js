import { StyleSheet } from 'react-native'
import Colors from 'src/Utils/Colors'

const Styles = StyleSheet.create( {
  navView: {
    backgroundColor: Colors.grey0,
    flexDirection: 'row',
    paddingHorizontal: 12,
    paddingVertical: 12,
  },
  navUiView: {
    flexDirection: 'row',
    paddingRight: 12,
    width: '33.33%'
  },
  navUiViewEnd: {
    flexDirection: 'column',
    paddingRight: 0,
  },
  navUiModeView: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  navButton: {
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderTopRightRadius: 4,
    flex: 1,
    paddingHorizontal: 8,
    paddingVertical: 4,
  },
  navButtonActive: {
    backgroundColor: Colors.tertiary,
  },
  navUiText: {
    color: Colors.grey3,
    fontFamily: 'DMSans_400Regular',
    fontSize: 12,
    textAlign: 'center',
    textTransform: 'capitalize'
  },
  navUiTextActive: {
    color: Colors.primary,
  },
  navUiInputIcon: {
    backgroundColor: Colors.tertiary,
    borderBottomLeftRadius: 4,
    borderTopLeftRadius: 4,
    height: 24,
    paddingHorizontal: 6,
    paddingVertical: 4,
    width: 25,
  },
  navUiIconText: {
    color: Colors.primary,
    fontFamily: 'DMSans_400Regular',
    fontSize: 12,
    textAlign: 'center',
  },
  navUiTextInput: {
    backgroundColor: Colors.white,
    borderBottomRightRadius: 4,
    borderTopRightRadius: 4,
    flex: 1,
    height: 24,
    paddingLeft: 6,
    paddingRight: 6,
    paddingVertical: 4,
    textAlign: 'left',
  },
} )

export default Styles
