import React, {
  useState,
} from 'react'
import {
  View,
  SafeAreaView,
  StyleSheet
} from 'react-native'

import { AppLoading } from "expo"
import {
  useFonts,
  DMSans_400Regular,
  DMSans_400Regular_Italic,
  DMSans_500Medium,
  DMSans_500Medium_Italic,
  DMSans_700Bold,
  DMSans_700Bold_Italic,
} from '@expo-google-fonts/dm-sans'
import { debounce } from 'lodash';

import Header from 'src/Components/Header'
import Nav from 'src/Components/Nav'
import Results from 'src/Components/Results'

import Styles from './styles'

 const Main = () => {

  // state
  const [ city, setCity ] = useState( 'london' )
  const [ mode, setMode ] = useState( 'latest' )
  const [ tags, setTags ] = useState( [] )

  // handles: mode, from: Nav
  function modeHandler() {
    setMode( prevMode => prevMode === 'latest' ? 'popular' : 'latest' )
  }

  // handles: city, from: Nav
  function cityHandler( input ) {
    setCity( input )
  }

  // handles: tags, from: Nav
  function tagsHandler( input ) {
    setTags( input && input.trim().split( ',' ).map( tag => tag.trim().replace( ' ', '-' ) ) )
  }

  // fonts loader
  const [ fontsLoaded ] = useFonts( {
      DMSans_400Regular,
      DMSans_400Regular_Italic,
      DMSans_500Medium,
      DMSans_500Medium_Italic,
      DMSans_700Bold,
      DMSans_700Bold_Italic,
  } )

  // returns AppLoading if: no fontsLoaded
  if ( !fontsLoaded ) {
    return <AppLoading />
  }

  const globalProps = {
    city,
    mode,
    tags,
  }

  // returns Main with SafeAreaView
  return (
    <SafeAreaView style={ Styles.main }>
      <Header { ...globalProps } />
      <Nav { ...globalProps }
        cityHandler={ debounce( cityHandler, 500 ) }
        tagsHandler={ debounce( tagsHandler, 500 ) }
        modeHandler={ modeHandler } />
      <Results { ...globalProps } />
    </SafeAreaView>
  )
}

export default Main
