import { StyleSheet } from 'react-native'
import Colors from 'src/Utils/Colors'

const Styles = StyleSheet.create( {
  main: {
    flex: 1,
    backgroundColor: Colors.primaryBackground,
  },
} )

export default Styles
