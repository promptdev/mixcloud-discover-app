import React from 'react'
import {
  Image,
  ImageBackground,
  StyleSheet,
  Text,
  View
} from 'react-native'

import PlayIcon from 'src/Assets/Svg/play.svg'

import Styles from './styles'

class Show extends React.PureComponent {
  render() {
    const show = this.props.showsData
    const tags = show.tags || []
    const picture = show.pictures.large
    const avatar = show.user.pictures.small

    return (
      <View style={ Styles.showView }>
        <View style={ Styles.showContentView }>
          <View>
            <ImageBackground source={{ uri: picture }} style={ Styles.showImage }>
              <View style={ Styles.showPlayIconView }>
                <View style={ Styles.showPlayIcon }>
                  <PlayIcon height={ 16 } width={ 20 } />
                </View>
              </View>
            </ImageBackground>
          </View>
          <View style={ Styles.showBodyView }>
            <Text numberOfLines={4} style={ Styles.showNameText }>
              { show.name }
            </Text>
            <View style={ Styles.showUserView }>
              <Image source={{ uri: avatar }} style={ Styles.showUserAvatarImage } />
              <Text numberOfLines={1} style={ Styles.showUserNameText }>
                by { show.user.name }
              </Text>
            </View>
            <View style={ Styles.showTagsView }>
              { tags.map( ( tag, i ) => {
                  return (
                    <View key={ `${ i }` } style={ Styles.showTagView }>
                      <Text style={ Styles.showTagText }>
                        #{ tag.name.replace( '#', '' ) }
                      </Text>
                    </View>
                  )
              } ) }
            </View>
          </View>
        </View>
      </View>
    )
  }
}

export default Show
