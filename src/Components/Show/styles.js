import { StyleSheet } from 'react-native'
import Colors from 'src/Utils/Colors'

const Styles = StyleSheet.create( {
  showView: {
    backgroundColor: Colors.white,
    flexDirection: 'row',
    height: 174,
    marginTop: 12,
    marginHorizontal: 12,
    padding: 12,
  },
  showContentView: {
    flex: 1,
    flexDirection: 'row',
  },
  showUserView: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  showUserAvatarImage: {
    borderBottomLeftRadius: 25,
    borderBottomRightRadius: 25,
    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    height: 20,
    marginRight: 6,
    width: 20,
  },
  showImage: {
    alignItems: 'center',
    flex: 1,
    height: 150,
    justifyContent: 'center',
    marginRight: 12,
    resizeMode: 'cover',
    width: 150,
  },
  showPlayIconView: {
    backgroundColor: Colors.white,
    borderColor: Colors.black,
    borderWidth: 2,
    borderBottomLeftRadius: 100,
    borderBottomRightRadius: 100,
    borderTopLeftRadius: 100,
    borderTopRightRadius: 100,
    height: 50,
    width: 50
  },
  showPlayIcon: {
    left: 15,
    top: 15,
  },
  showBodyView: {
    flex: 1,
  },
  showNameText: {
    color: Colors.black,
    fontFamily: 'DMSans_700Bold',
    fontSize: 16,
    lineHeight: 18,
    marginBottom: 4,
    paddingTop: 4
  },
  showUserNameText: {
    color: Colors.grey3,
    flex: 1,
    fontFamily: 'DMSans_500Medium',
    fontSize: 12,
    lineHeight: 18,
  },
  showTagsView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 'auto',
    maxHeight: 47,
    overflow: 'hidden'
  },
  showTagView: {
    borderColor: Colors.secondary0,
    borderWidth: 1,
    borderBottomLeftRadius: 2,
    borderBottomRightRadius: 2,
    borderTopLeftRadius: 2,
    borderTopRightRadius: 2,
    marginBottom: 4,
    marginRight: 4,
    paddingHorizontal: 4,
    paddingVertical: 2,
  },
  showTagText: {
    color: Colors.secondary3,
    fontFamily: 'DMSans_400Regular',
    fontStyle: 'italic',
    fontSize: 10,
    textTransform: 'lowercase'
  }
} )

export default Styles
