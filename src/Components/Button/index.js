import React from 'react'
import {
  ActivityIndicator,
  Text,
  TouchableOpacity
} from 'react-native'

import Colors from 'src/Utils/Colors'

const Button = props => {
  return (
    <TouchableOpacity
      onPress={ props.onPress || {} }
      style={ props.buttonStyle || {} }>
      <Text style={ props.buttonTextStyle || {} }>
        { props.buttonText || 'Button Label' }
      </Text>
      { props.loading ? (
        <ActivityIndicator
          color={ props.activityIndicatorColor || Colors.black }
          style={ props.activityIndicatorStyle || { marginLeft: 12 } } />
      ) : null }
    </TouchableOpacity>
  )
}

export default Button
