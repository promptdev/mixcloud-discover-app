import React, {
  useState,
  useEffect,
} from 'react'
import {
  View,
  VirtualizedList,
  Text,
} from 'react-native'

import Api from 'src/Utils/Api'
import Fetch from 'src/Utils/Fetch'

import Button from 'src/Components/Button'
import Loader from 'src/Components/Loader'
import Show from 'src/Components/Show'

import Styles from './styles'

const Results = ( {
  city,
  mode,
  tags
} ) => {

  // state
  const [ connections, setConnections ] = useState( {} )
  const [ loading, setLoading ] = useState( true )
  const [ nextPage, setNextPage ] = useState( '' )
  const [ showsData, setShowsData ] = useState( null )
  const [ title, setTitle ] = useState( null )

  // effects: connections, with: city, mode, tags
  useEffect( () => {
    const getConnections = async () => {
      setShowsData( null )
      const clearTags = tags && tags.filter( tag => tag.trim() != '' ).map( tag => tag.trim() )
      const clearCity = city.trim().replace( ' ', '-' ).toLowerCase()
      const discoverData = await Fetch( Api.discover( { city: clearCity, tags: clearTags } ) )

      setTitle( discoverData.name )
      setConnections( discoverData.metadata.connections )
    }

    city && getConnections()
  }, [ city, mode, tags ] )

  // effects: showsData, with: connections
  useEffect( () => {
    async function getSowsData() {
      const showsData = await Fetch( connections[ mode ] )

      setShowsData( showsData.data )
      setNextPage( showsData.paging?.next )
      setLoading( false )
    }

    getSowsData()
  }, [ connections ] )

  // loadMore button handler
  const loadMore = async () => {
    // exit if: no nextPage
    if ( !nextPage ) return

    setLoading( true )
    const nextPageShowsData = await Fetch( nextPage )

    setNextPage( nextPageShowsData.paging.next )
    setShowsData( prevShowsData => [ ...prevShowsData, ...nextPageShowsData.data ] )
    setLoading( false )
  }

  // renders ListFooterComponent with: Button
  const renderListFooter = () => {
    // exit if: no nextPage, render blank
    if ( !nextPage ) return( <></> )

    return(
      <Button
        onPress={ loadMore }
        buttonStyle={ Styles.buttonStyle }
        buttonTextStyle={ Styles.buttonTextStyle }
        buttonText='Load More'
        loading={ loading } />
    )
  }

  const renderHeader = () => {
    return(
      <View style={ Styles.headerView }>
        <Text style={ Styles.headerText }>
          { title }
        </Text>
      </View>
    )
  }

  // returns Loader if no: connections, showsData
  if ( !showsData ) {
    return ( <Loader /> )
  }

  // returns Flatlist of Shows items
  return (
    <>
    <VirtualizedList
      data={ showsData }
      getItem={ ( shows, i ) => shows[ i ] }
      getItemCount={ data => data.length }
      keyExtractor={ ( item, i ) => `${ item.slug }${ i }` }
      initialNumToRender={ 4 }
      renderItem={ data => <Show showsData={ data.item } /> }
      ListFooterComponent={ renderListFooter }
      ListHeaderComponent={ renderHeader } />
    </>
  )
}

export default Results
