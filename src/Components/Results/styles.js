import { StyleSheet } from 'react-native'
import Colors from 'src/Utils/Colors'

const Styles = StyleSheet.create( {
  headerView: {
    flex: 1,
    paddingHorizontal: 12,
    paddingTop: 12,
  },
  headerText: {
    color: Colors.text,
    fontFamily: 'DMSans_700Bold',
    fontSize: 20,
  },
  buttonStyle: {
    alignItems: 'center',
    alignSelf: 'center',
    backgroundColor: Colors.tertiary,
    borderBottomLeftRadius: 4,
    borderBottomRightRadius: 4,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 24,
    padding: 12,
    width: '66.66%',
  },
  buttonTextStyle: {
    color: Colors.primary,
    fontFamily: 'DMSans_500Medium',
  }
} )

export default Styles
