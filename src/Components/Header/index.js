import React from 'react'
import {
  Text,
  View
} from 'react-native'

import MixcloudLogo from 'src/Assets/Svg/mixcloud.svg'

import Styles from './styles'

class Header extends React.PureComponent {

  render() {
    const { city } = this.props

    return (
      <View style={ Styles.headerView }>
        <View style={ Styles.headerTitleView }>
          <MixcloudLogo height={ 14 } width={ 119 } />
        </View>
        <Text style={ Styles.headerText }>
            Discover
        </Text>
        <Text style={ Styles.promptText }>
            &nbsp;{ city.replace( '-', ' ' ) }
        </Text>
      </View>
    )
  }
}

export default Header
