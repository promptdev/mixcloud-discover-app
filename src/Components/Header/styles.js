import { StyleSheet } from 'react-native'
import Colors from 'src/Utils/Colors'

const Styles = StyleSheet.create( {
  headerView: {
    alignItems: 'center',
    backgroundColor: Colors.primary,
    flexDirection: 'row',
    padding: 12,
  },
  headerText: {
    color: Colors.white,
    fontFamily: 'DMSans_500Medium',
    fontSize: 18,
    marginLeft: 8,
  },
  promptText: {
    color: Colors.white,
    fontFamily: 'DMSans_500Medium',
    fontSize: 18,
    textTransform: 'capitalize'
  }
} )

export default Styles
