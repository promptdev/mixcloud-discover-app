import { StyleSheet } from 'react-native'
import Colors from 'src/Utils/Colors'

const Styles = StyleSheet.create( {
  loaderView: {
    alignItems: 'center',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  loaderText: {
    color: Colors.primary,
    fontFamily: 'DMSans_400Regular',
    marginTop: 6,
  },
} )

export default Styles
