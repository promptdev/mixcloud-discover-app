import React, { Component } from 'react'
import {
  ActivityIndicator,
  Text,
  View,
} from 'react-native'

import Colors from 'src/Utils/Colors'

import Styles from './styles'

const Loader = () => {
  return (
    <View style={ Styles.loaderView }>
      <ActivityIndicator size="large" color={ Colors.grey3 } />
      <Text style={ Styles.loaderText }>
        Loading...
      </Text>
    </View>
  )
}

export default Loader
