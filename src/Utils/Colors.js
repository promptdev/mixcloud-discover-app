const Colors = {
  white: '#fff',
  black: '#212121',
  grey0: '#ebf0f2',
  grey1: '#BAC2C8',
  grey2: '#89939D',
  grey3: '#586573',
  text: '#49596a',
  primary: '#1e2337',
  primaryBackground: '#f5f9fc',
  secondary0: '#f2eef8',
  secondary1: '#b197d6',
  secondary2: '#7d52ba',
  secondary3: '#7d52ba',
  tertiary: '#f3b2a6',
}

export default Colors
