const Constants = {
  statusCodes: {
    SUCCESS: 200,
    CREATED: 201,
    ACCEPTED: 202,
    REDIRECT: 300,
    TEMPORARY_REDIRECT: 307,
    PERMANENT_REDIRECT: 308,
    BAD_REQUEST: 400,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,
    NOT_FOUND: 404
  },
}

export default Constants
