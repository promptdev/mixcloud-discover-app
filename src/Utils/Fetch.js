import Constants from 'src/Utils/Constants'

const Fetch = ( uri ) => {
  return fetch( uri )
    .then( response => {
      if ( response.ok && response.status === Constants.statusCodes.SUCCESS ) {
        return response.json()
      }
      throw new Error( 'API call failed, check request config' )
    } )
    .then( json => {
      return json
    } )
    .catch( error => {
      if ( error ) return error
    } )
}

export default Fetch
