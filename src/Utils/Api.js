import Config from 'src/Config'

const Api = {

  /*
   * Discover endpoint

   * by city
   * https://api.mixcloud.com/discover/city:london/?metadata=1

   * by city connections
   * https://api.mixcloud.com/discover/city:london/popular/
   * https://api.mixcloud.com/discover/city:london/latest/

   * by tag connections
   * https://api.mixcloud.com/discover/disco/popular/
   * https://api.mixcloud.com/discover/rap+hip-hop/latest/

   * by city & tags connections
   * https://api.mixcloud.com/discover/city:london+disco/popular/
   * https://api.mixcloud.com/discover/city:london+rap+hip-hop/latest/

   */
  discover: ( options = {} ) => {
    if ( typeof options !== 'object' && options.constructor !== Object && options !== null ) {
      return
    }

    // options
    const city = options.city ? `city:${ options.city }` : ''
    const tags = options.tags?.length > 0 ? options.tags : []
    const metadata = options.hasOwnProperty( 'metadata' ) ? options.metadata : true
    const query = [ city, ...tags ].join( '+' )

    return `${ Config.API_URL }/discover/${ query }${ metadata ? '?metadata=1' : '' }`
  }
}

export default Api
