# Mixcloud Discover

App that lets you discover [Mixcloud](https://www.mixcloud.com) shows by using [Mixcloud Discover Api](https://www.mixcloud.com/developers/). Search by city or hashtag and browse all the latest shows. Switch mode to see the most popular ones.

This app was created using [Expo](https://expo.io).

### Install Dependencies
`npm install`

### Run the app
`npm start`
